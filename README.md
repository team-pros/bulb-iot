### Połączenie programatora z Launchpad do naszej płyty głównej ###

Opisy góra/dół lewo/prawo odnoszą się do sytuacji gdy:
* Launchpad ułożony jest w taki sposób że złącze USB jest na górze płytki gdy patrzymy do strony procesora
* Nasza płyta ułożona jest w taki sposób że płyta-córka jest na dole

* Odłączyć urządzenie od zasilania
* Odłączyć płytę-córkę od płyty głównej (grube przewody mogą zostać, chodzi tylko o odsłonięcie złącza LEFT)
* Założyć zworkę na dwa piny które znajdują się zaraz na prawo od modułu CC3200, najbliżej dolnej krawędzi płyty)
* Podączyć Launchpada do naszej płyty cztrerama liniami w następujący sposób:
* Launchpad ----------------------------------- sygnał ------ nasza płyta
* Złącze J6, górny pin (1cm nad procesorem) --- UART Rx ----- złącze LEFT, 3 pin od dołu
* Złącze J7, górny pin (1cm nad procesorem) --- UART Tx ----- złącze LEFT, 2 pin od dołu
* Złącze J19 pin GND (lewy dolny róg) --------- GND --------- złącze LEFT, 1 pin od dołu
* Złącze P2 pin RST (na prawo od procesora) --- nRESET ------ pin RESET nad złączem LEFT 

* Podłączyć urządzenie do zasilania
* Wykonać na kompie to co jest opisane poniżej (Dla userów)
* Odłączyć urządzenie od zasilania
* Zdjąć zworkę 
* Podłączyć urządzenie do zasilania

### Oglądanie informacji debugowych na kompie ###

* Podłączyć naszą płytę z Launchpadem tak jak to jest opisane powyżej
* Podłączyć Launchpad do kompa
* Wyszukać w systemie na którym wirtulanym COMie widziany jest launchpad
* Uruchomić jakiś terminal (np. TeraTerm)
* Ustawić odpowiedni COM
* Ustawić prędkosć na 115200.
* Podłączyć zasilanie do naszej płyty
* Na terminalu powinna pojawić się nazwa aplikacji i inne informacje


### Dla userów ###

* Ustawić się tam gdzie ma być ściągnięte repozytorium, ten katalog będę nazywał: ProsBulbRepo
* Ściągnąć repozytorium: git clone https://bitbucket.org/team-pros/bulb-iot.git
* Uruchomić CCS UniFlash (http://processors.wiki.ti.com/index.php/CCS_UniFlash_v3.4.1_Release_Notes) 
* Wybrać "Open target configuration"
* Nacisnąć "Browse"
* Wskazać na: <ProsBulbRepo>/dodatkowe_plik/CCS UniFlash config.usf
* Nacisnąć "OK"
* Wybrać "CC31xx/CC32xx Flash Setup and Control"
* Wyedytować "COM Port", ustawić ten numer portu gdzie jest podłączony programator Launchpad (CC3200LP Dual Port)
* Wybrać "System Files -> /sys/mcuimg.bin"
* Pole Url powinno wskazywać na: <ProsBulbRepo>\ProsBulb_PROS.bin  (Obraz naszego programu dla naszej płyty)
* Ewentualnie zmienić na: <ProsBulbRepo>\ProsBulb_Launchpad.bin  (Obraz naszego programu dla płyty Launchpad)
* Zaznaczyć Erase, Update oraz Verify  (powinno być zaznaczone)
* Aby zmienić typ albo id urządzenia wyedytować plik <ProsBulbRepo>/dodatkowe_plik/id.txt
*   Plik ten jest postaci: <typ_urzadzenia>_<id_urzadzenia> bez <CR><LF>   (np: "bulb_2345" lub "dim_0034")
* Aby podać od razu namiary na sieć (żeby nie było potrzeby uruchamiać smartconfiga na telefonie) należy:
*   Wybrać CC31xx/CC32xx Config Groups ->  Profiles , zaznaczyć Update oraz Enable Profile 1
*   Wybrać CC31xx/CC32xx Config Groups ->  Profiles ->  Profile 1 
*   Ustawić Profile 1 SSID = <nazwa_sieci> , Profile 1 Priority = 1 , Profile 1 Security Type = <typ zabezpieczenia> oraz odpowiedni klucz poniżej
*   Wybrać "CC31xx/CC32xx Flash Setup and Control"
*   Upewnić się że na CC3200-LAUNCHXL zworki od bootowania ustawione są na "100:FLASH"
*   Nacisnąć przycisk "Format" i wybrać 1MB
* Upewnić się że na CC3200-LAUNCHXL zworki od bootowania ustawione są na "100:FLASH"
* Nacisnąć przycisk "Program"
* W dolnym oknie (Console) pojawią się komunikaty, ostatni z nich powinien być "Operation Program returned"
* Na CC3200-LAUNCHXL zworki bootowania ustawić na "000:4W JTAG"
* Na CC3200-LAUNCHXL nacisnąć switch SW1 - RESET.

### Dla developerów ###

* cd <CCS_workspace>
* git clone https://bitbucket.org/team-pros/bulb-iot.git
* Uruchomić Code Composer Studio
* Project > Import CCS Projects... 
* Select search-directory > Browse
* Wybrać katalog do którego był sklonowany project 
* Wybrać "ProsBulb"
* Kliknąć "Finish"
* Projekt korzysta z plików nagłówkowych oraz bibliotek które znajdują
   się w SDK dla CC3200, aby CCS wiedział gdzie je znaleźć należy 
   ustawić zmienną CC3200_SDK dla całego workspace
* Window > Preferences > Code Cmposer Studio > Build > Variables
   Name = CC3200_SDK
   Type = String
   Value = <katalog_z_SDK_dla_CC3200> (u mnie C:\TI\CC3200SDK_1.1.0\cc3200-sdk\)
* Teraz już powinno się budować