//
//
//  Na razie jest tak �e je�li flash jest swiezo po formatowaniu to wszytsko jest ok
//  Jezeli we flashu jest tylko aplikacja /sys/mcuimg.bin to te� jest wszystko ok.
//  Je�li natomiast probuje cos ustawic u�ywajc UniFlasha (np. w sekcji mDNS co powoduje zapisanie plikow /sys/devname.cfg i /sys/mdns.cfg)
//   to aplikacja przestaje dzialac (niezale�nie od tego czy jest ona uruchamiana z flahsha czy poprzez CCS
//   Przestaje dzialac tzn utyka na funkcji sl_start().


//  ***** HARDWARE *****
//
// Po��czenie Launchpad do naszej p�ytki wykonawczej:
//
//          Launchpad                                              p�ytka wykonawcza
//  z��cze lewe P57 (GPIO A0.2)   -  zero crossing  -  br�zowy   -   LEFT.3        (zwora J6 ma by� w pozycji BP)
//  z��cze prawe P18 (GPIO A3.4)  -  dimmer signal  -  niebieski -   RIGHT.3
//  Vcc                           -  Vcc            -  czerwony  -   RIGHT.1
//  GND                           -  GND            -  czarny    -   RIGHT.5
//
// Po��czenie programatora z Launchpad do naszej p�yty g��wnej:
//
//    programator                                 nasza p�yta
//  J6.g�rny pin      -   UART Rx   -  zielony  -  LEFT.3
//  J7.g�rny pin      -   UART Tx   -  bia�y    -  LEFT.4
//  GND               -   GND       -  czarny   -  LEFT.5
//  z��cze prawe RST  -   nRESET    -  brazowy  -  switch RESET obok z��cza LEFT


//http://processors.wiki.ti.com/index.php/Portable_Projects
// Tak jest w CCS 6.1.2:
// Ustawianie zmiennych dla projektu:   Project > Properties > Resource > Linked resource > Path variables
// Ustawianie zmiennych dla ca�ego CCS: Window > Preferences > Code Cmposer Studio > Build > Variables

// Na poziomie CCS nale�y ustawi� zmienn�: CC3200_SDK aby wskazywa�a na katalog gdzie jest SDK dla CC3200
// (u mnie jest to: C:\TI\CC3200SDK_1.1.0\cc3200-sdk\)

//  ***** CHANGELOG *****
// 2016.12.04 Automatyczne ��czenie do sieci zapamietanej w profilach. Na przycisk roz�aczanie i nas�uchiwanie smartconfiga
// 2016.12.04 Jak nie ma �adnej zapami�tanje sieci w profilach to od razu przechodzi do samrtconfiga
// TODO (co jak jest siec ale nie mog� do niej polaczyc)
// TODO (co jak moge polaczyc do sieci ale nie dostaje adresu IP od DHCP)
// TODO (co jak nacisne przycisk reset gdy jest w smartconfigu?)
// 2016.12.04 Dodana konfiguracja "PROS_debug" i "PROS_release" kt�re buduja aplikacj� dla naszej plyty
// 2016.12.17 Odczytywanie z pliku id i typu urz�dzania  (co z nazw�?)
//            Plik znajduje si� w: d:\pros\TI_projekty\id.txt  ->  /pros/id.txt
// 2016.12.17 wysy�anie w mdns id i typu (a mo�e i nazwy)
// 2016.12.17 Dodana wersj programu ktora jest wypluwana przy starcie na RS232
// po stronie aplikacji na komorce wyluskac typ i na tej podstawie rozny interfejs
// gdzie zapamietywana nazwa???
// 2017.03.12 Dodane zapalanie/gaszenie lampy przez http post (na razie tylko launchpad)
// 2017.04.02 Zaimplementowane wykrywanie zboczy na linii "zero crossing" (GPIO A0.2 (P57))  (migam diod� popmaranczow� co 100 zboczy (1sek))
//            UWAGA: Aby to dzia�a�o pin LEFT.C lightboard musi by� pod��czont do P57 launchpada a zwora J6 musi by� ustawiona w pozycj� BP
// 2017.04.02 Dodana obs�uga �ciemniania lampy przez http post (POST 192.168.2.102/bulb/api/v1/dim=50)
// 2017.04.03 Dorobi�em przew�d ��czacy  lightboard RIGHT.C  z  pinem P18 launchpad abym m�g� sterowa� �wiat�em
//       z launchpada
// 2017.04.03 Przew�d ��czacy lightboard LEFT.A,LEFT.C,LEFT.E z  launchpad  Vcc,P57,GND przelutowa�em tak aby
//        zasilanie by�o czerwone a masa czarna
// 2017.04.03 Doda�em do programu sterownnie triakiem ale na razie �ar�wka si� nie zapala. Sprawdzi�.
// 2017.04.04 Uruchomione �ciemnianie i zapalanie lampy przez launchpad
// 2017.06.10 Odczytywanie statusu lampy przez http get, status zwracany w ciele http jako json:
//              {
//            	 "type": "bulb"
//            	 "id": "0001"
//				 "ip": "192.168.1.1"
//				 "dim": 58
//              }
//            Aby status byl zwracany musi by� w cc3200 zapisany plik: /www/bulb/api/v1/status w kt�rym warto�ci zmienne s� postaci: __SL__G_XYZ
//           i jak simplelink trafi na taki token to jest wywo�ana funcja httpservercallback i podstawia odpowiednia wartosc
//           Plik statusu znajduje si� w: d:\pros\TI_projekty\status.txt  ->  /www/bulb/api/v1/status
// 2017.06.24 Dodane do repozytorium pliki id.txt i status.txt. S� w katalogu dodatkowe_pliki.
// 2017.06.24 Dodany plik .ucf do repozytotium (to ten plik dla CCS UniFlash kt�ry m�wi co nale�y przegra� do CC3200). Jest w katalogu dodatkowe_pliki
// 2017.06.24 Nie wiem dlaczego ale przesta� dobrze dzia�a� mDNS, doda�em stopopwanie i startowanie tej us�ugi i pomog�o.
// 2017.06.24 Przy okazji zmieni�em troch� Smartconfig plugin dla cordovy tak aby wypluwa� wi�cej informacji debugowej (wypluwa konunikat
//             o tym �� znalaz� jakie� urz�dzenie nawet gdy nie jest to urz�dzenie kt�re w mdns zwraca "pros_id"
// 2017.06.24 Uruchomi�em program na naszej p�ycie, okaza�o si� �e �ciemnianie niedzia�� dobrze (�ar�wka "mruga")
// 			Chocia� teoretycznie powinienem dostawa� przerwania tylko od rising edge linii "zero crossing" w rzeczywisto�ci wygl�da, �e
// 			dostawa�em te� przerwania od falling edge, co powodowa�o �e czasy by� odliczane od r�nych chwil
// 			Na p�ycie na kt�rj testowa�em by�o tak �e odleg�o�ci pomi�dzy kolejnymi przerwaniami by�y: 9.2ms lub 10ms lub 10.8ms
// 			co oznacza �e "szpilka" od detekcji zera trwa�a 0.8ms i raz czasy by�y odmierzane od r�nych moment�w co by�o wida� na �wiecenie �ar�wki
// 			Trzeba by�o jako� spowodowa� �e liczy� zawsze od tego samego zbocza.
// 			Zrobi�em to w ten spos�b (chocia� pewnie nie jest to najlepsze rozwi�zanie), �e od z�apania pzerwania "zero crossing" blokuj� przerwania od
// 			tej lini a� na 9,8 ms (wcze�niej mai�em ten czas ustawiony na 1,5ms) co powoduje �e jak chocia� raz z�api� przerwanie od zbocza opadaj�cego
// 			to ju� wszystkie kolejne przerwania b�d� te� od zbocza opadaj�cego. Dzi�ki temu odleg�o�ci pomi�dzy kolejnymi zboczami s� teraz zawsze takie same
// 			ale trzeba by�o te� zmieni� moment gaszenia linii "dimmer" bo jak gasi�em j� na zboczy opadaj�cym "zero crossing" to ju� by�o za p�no i
// 			�ar�wka �wieci�a si� ca�y czas. Teraz linia "dimmer" jest gaszona zawsze po 0.1ms od jej zapalenia (u�ywam do odmierzenia tego czasu ten
// 			sam timer co u�ywam do odmierzania czasu pomi�dzy "zero crossing" a zapalaniem linii "dimmer"
// 2017.06.24 Dodany #define DEBUG_PRZERWANIA - po w��czeniu wypluwa informacje na temat przerwa�. Patrz opis przy tym definie.



//***********************************************************************************************************************
// include
//***********************************************************************************************************************

// <CC3200_SDK>/inc
#include "hw_types.h"   // typ tBoolean oraz makra typu HWREG()
#include "hw_ints.h"    // numery fauli i przerwa�  FAULT_XXX i INT_XXX
#include "hw_memmap.h"  // adresy poszczeg�lnych peryferi�w

// <CC3200_SDK>/driverlib
#include "rom_map.h"  // includuje funkcje z biblioteki "driverlib" lub funkcj� kt�ra jest w ROMie mikrokontrolera
#include "interrupt.h" // driver dla NVIC - wszystkie funkcje IntXXXXXXX()
#include "prcm.h"    // driver dla PRCM (Power, Reset and Clock Module) - wszystkie funkcje PRCMXXXXXX()
#include "gpio.h"   // driver dla GPIO - funkcje GPIOXXXX()
#include "pin.h"    // driver dla modu�u pinmux  - funkcje PinXXXXX()
#include "utils.h"  // funkcja UtilsDelay()
#include "uart.h"   // driver dla UARTa - funkcje UARTXXXXX()
#include "timer.h"  // driver dla timer�w - funkcje TimerXXXXX()

// <CC3200_SDK>/simplelink/include
#include "simplelink.h"   // wszystkie funkcje sieciowe - funkcje sl_XXXXXXX()


// standardowe biblioteki
#include <stdio.h>   // sprintf
#include <string.h>  // strcat




//***********************************************************************************************************************
// define
//***********************************************************************************************************************

#define WERSJA_FIRMWARE "v0.1.4"

#ifdef TARGET_LAUNCHPAD

#define ZapalCzerwony()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1, 0xFF)
#define ZgasCzerwony()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1, 0)
#define ZapalPomaranczowy()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_2, 0xFF)
#define ZgasPomaranczowy()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_2, 0)
#define ZapalZielony()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_3, 0xFF)
#define ZgasZielony()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_3, 0)

#define ZapalWszystkie()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0xFF)
#define ZgasWszystkie()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0)

#define ZapalDiodeStatus()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_3, 0xFF)
#define ZgasDiodeStatus()  MAP_GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_3, 0)

#define CzytajPrzycisk()  ( MAP_GPIOPinRead(GPIOA1_BASE, GPIO_PIN_5) >> 5)

#endif


#ifdef TARGET_PROS

void no_op() {};

#define ZapalCzerwony() no_op()
#define ZgasCzerwony()  no_op()
#define ZapalPomaranczowy()  no_op()
#define ZgasPomaranczowy()  no_op()
#define ZapalZielony()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0xFF)
#define ZgasZielony()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0)

#define ZapalWszystkie()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0xFF)
#define ZgasWszystkie()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0)

#define ZapalDiodeStatus()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0xFF)
#define ZgasDiodeStatus()  MAP_GPIOPinWrite(GPIOA2_BASE, GPIO_PIN_6, 0)

#define CzytajPrzycisk()  ( (~(MAP_GPIOPinRead(GPIOA0_BASE, GPIO_PIN_7)) & 0x80 ) >> 7)
#endif






//#define SERVICE_NAME            "PROS_bulb._http._tcp.local"      // nazwa widziana w sici po mDNS
#define SERVICE_NAME_LENGTH 30



#define SSID_LEN_MAX        32
#define BSSID_LEN_MAX       6



// z punktu widzenia ��czno�ci mi�dzy kom�rk� a CC3200 mo�emy znajdowa� si� w jednym z 4 stan�w
#define POLACZENIE_NASLUCHUJE 0
#define POLACZENIE_LACZY 1
#define POLACZENIE_CZEKA_NA_IP 2
#define POLACZENIE_NAWIAZANE 3

#define TYP_URZADZENIA_LEN_MAX 15
#define ID_URZADZENIA_LEN_MAX 15
#define ADRES_IP_LEN_MAX 20


// Ustawienie DEBUG_PRZERWANIA powoduje �e:
// wej�cie w przerwanie od "zero crossing" jest rejestrowane w tablicy "debug_gdzie" oraz zapisywane s� stany timer�w A0 i A1
// wej�cie w przerwanie od Timera A0 (czas do zapalenia i zgaszenia lini "dimmer") jest rejestrowane w tablicy "debug_gdzie" oraz zapisywane s� stany timer�w A0 i A1
// wej�cie w przerwanie od Timera A1 (czas zamaskowania przerwania od "zero crossing") jest rejestrowane w tablicy "debug_gdzie" oraz zapisywane s� stany timer�w A0 i A1
// gdy w p�tli g��wenej oka�e si� �e jest zape�niona tablica "debug_gdzie" zawarto�ci tej tablicy wypluwana jest na rs232
// Dane debugowe:
//  <gdzie> <stan timera A0> <stan timera B0>
// stany timer�w podane s� w us. Obydwa timery s� one shot i odliczaj� w d�. po tym jak odlicz� do zera ustawiane s� warto�ci� pocz�tkow� i generowane jest przerwanie
// gdzie == 1 - przerwanie "zero crossing" zaraz po wystartowaniu Timera A1
// gdzie == 2 - przerwanie "zero crossing" po wystartowaniu timera A0
// gdzie == 3 - przerwanie od Timera A1  (przerwanie odmaskowuj�ce przerwania od "zero_crossing")
// gdzie == 4 - przerwanie od Timera A0 gdy g_stan_triaka == 0 (przerwanie zapalaj�ce linie "dimmer")
// gdzie == 5 - przerwanie od Timera A0 gdy g_stan_triaka == 1 (przerwanie gasz�ce lini� "dimmer")

//#define DEBUG_PRZERWANIA




//***********************************************************************************************************************
// Sta�e globane
//***********************************************************************************************************************

const char g_ccServiceName[] = "._http._tcp.local";


//***********************************************************************************************************************
// Zmienne globane
//***********************************************************************************************************************
extern void (* const g_pfnVectors[])(void);    // tablica wektor�w przerwa� - definicja w startup_ccs.c


unsigned char  g_ucConnectionSSID[SSID_LEN_MAX+1]; //Connection SSID (musimy zapamietywac?)
unsigned char  g_ucConnectionBSSID[BSSID_LEN_MAX]; //Connection BSSID (musimy zapamietywac?)


unsigned char g_wypelnienie = 0;   // aktualny poziom �wiecenia wyra�ony w procentach: 0 - nie �wieci, 100 - �wieci na maksa
int g_wypelnienie_nastawa = 0;    // aktualny poziom �wiecenia, warto�� kt�r� nale�y wpisa� do timera odpalanego w przerwaniu "zero crossing"
                                 // zdecydowa�em si� na zmienna globalna aby nie liczyc tego co 10ms w przerwaniu
unsigned char g_polaczenie = POLACZENIE_NASLUCHUJE ; // aktualny stan ��czno�ci

char g_cServiceName[SERVICE_NAME_LENGTH+17+1];    // nazwa po kt�rej b�dzie widziany w sieci modu� przez mDNS (maksymalnie 30 bajt�w na cz�� zmienn�)


unsigned char g_TypUrzadzenia[TYP_URZADZENIA_LEN_MAX+1]; // to w�a�ciwie powina by� sta�a ale na razie jest tak �e typ urz�dzenia
                                                     // mo�na ustwai� poprzez edycj� pliku /pros/id.txt
unsigned char g_IDUrzadzenia[ID_URZADZENIA_LEN_MAX+1]; // zapisane w /pros/id.txt
unsigned char g_AdresIP[ADRES_IP_LEN_MAX+1];      // adres IP urz�dzenia przyznany przez serwer DHCP


char g_stan_triaka = 0;     // 1 - triak wysterowany    // 0-triak nie wysterowany
                           // jako �e jedno przerwanie odpowiada za gaszenie i zapalanie triaka to musze wiedziec w jakim stanie jest aktualnie linia
                           // m�g�bym pewnie czyta� stan linii ale nie chcia�o mi si� sprawdza� czy jest do tego API




//***********************************************************************************************************************
// Definicje funkcji
//***********************************************************************************************************************





//*****************************************************************************
// Funkcja pod��czaj�ca zegar do tych peryferi�w kt�re s� wykorzystywane w programie

void ClockConfig(void)
{

	// UARTA0 - terminal
	MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);

	// GPIOA0 - switch reset, zero crossing
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);

	// GPIOA1 - ledy i switch reset
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);

	// GPIOA2 - led zielony, SW2
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA2, PRCM_RUN_MODE_CLK);

	// GPIOA3 - dimmer signal
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);

	// TimerA0 - na razie do cel�w debugowych
	// Docelowo moze byc wykorzystany do odmierzania czasu miedzy detekcj� zera a za��czeniem zasialani
	MAP_PRCMPeripheralClkEnable(PRCM_TIMERA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralReset(PRCM_TIMERA0); // czy to jest potrzebne???

	// TimerA1 -  do odliczania czasu miedzy zadzia�aniem przerwania od lini detekcji zera
	// do momentu ponownego odblokowania tego przerwania
	// (nie mo�na od razu odblokowa� lini detekcji zera bo dostaj� kilka przerwa� w jednym cyklu)
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA1, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralReset(PRCM_TIMERA1);

}




//*****************************************************************************
void UARTConfig()
{

  MAP_UARTConfigSetExpClk(UARTA0_BASE, MAP_PRCMPeripheralClockGet(PRCM_UARTA0), 115200,
		                  (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE) );
}


//*****************************************************************************
void GPIOConfig()
{

#ifdef TARGET_LAUNCHPAD
	  // PIN_64 jako GPIO9 (GPIOA1 pin 1) - led czerwony
	  MAP_GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_1, GPIO_DIR_MODE_OUT);

	  // PIN_01 jako GPIO10 (GPIOA1 pin 2) - led pomara�czowy
	  MAP_GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_2, GPIO_DIR_MODE_OUT);

	  // PIN_02 jako GPIO11 (GPIOA1 pin 3) - led zielony
	  MAP_GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_3, GPIO_DIR_MODE_OUT);

	  // PIN_04 jako GPIO13 (GPIOA1 pin 5) - switch SW3 - switch reset
	  MAP_GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_5, GPIO_DIR_MODE_IN);

	  // PIN_15 jako GPIO22 (GPIOA2 pin 6)- switch SW2
	  MAP_GPIODirModeSet(GPIOA2_BASE, GPIO_PIN_6, GPIO_DIR_MODE_IN);

	  //// PIN_18 jako GPIO28 (GPIOA3 pin 4) - zero crossing detection
	  //MAP_GPIODirModeSet(GPIOA3_BASE, GPIO_PIN_4, GPIO_DIR_MODE_IN);

	  // PIN_57 jako GPIO2 (GPIOA0 pin 2) - zero crossing detection  (kolizja z USARTA0.RX)
	  MAP_GPIODirModeSet(GPIOA0_BASE, GPIO_PIN_2, GPIO_DIR_MODE_IN);

	  // PIN_18 jako GPIO28 (GPIOA3 pin 4) - dimmer signal
	  MAP_GPIODirModeSet(GPIOA3_BASE, GPIO_PIN_4, GPIO_DIR_MODE_OUT);

#endif


#ifdef TARGET_PROS
	  // PIN_15 jako GPIO22 (GPIOA2 pin 6) - led zielony
	  MAP_GPIODirModeSet(GPIOA2_BASE, GPIO_PIN_6, GPIO_DIR_MODE_OUT);

	  // PIN_62 jako GPIO7 (GPIOA0 pin 7) - switch reset
	  MAP_GPIODirModeSet(GPIOA0_BASE, GPIO_PIN_7, GPIO_DIR_MODE_IN);

	  // PIN_57 jako GPIO2 (GPIOA0 pin 2) - zero crossing detection  (kolizja z USARTA0.RX)
	  MAP_GPIODirModeSet(GPIOA0_BASE, GPIO_PIN_2, GPIO_DIR_MODE_IN);

	  // PIN_18 jako GPIO28 (GPIOA3 pin 4) - dimmer signal
	  MAP_GPIODirModeSet(GPIOA3_BASE, GPIO_PIN_4, GPIO_DIR_MODE_OUT);
#endif



}




//*****************************************************************************
// Funkcja kt�ra konfiguruje piny mikrokontolrera tak aby na wyjscie trafialy odpowiednie peryferia
void PinMuxConfig(void)
{

#ifdef TARGET_LAUNCHPAD
    // PIN_64 jako GPIO9 - led czerwony
    MAP_PinTypeGPIO(PIN_64, PIN_MODE_0, false);    // MODE_0 -> GPIO9 (GPIOA1 pin 1)

    // PIN_01 jako GPIO10 - led pomara�czowy
    MAP_PinTypeGPIO(PIN_01, PIN_MODE_0, false);    // MODE_0 -> GPIO10 (GPIOA1 pin 2)

    // PIN_02 jako GPIO11 - led zielony
    MAP_PinTypeGPIO(PIN_02, PIN_MODE_0, false);    // MODE_0 -> GPIO11 (GPIOA1 pin 3)

    // PIN_04 jako GPIO13 - switch SW3
    MAP_PinTypeGPIO(PIN_04, PIN_MODE_0, false);    // MODE_0 -> GPIO13 (GPIOA1 pin 5)

    // PIN_15 jako GPIO22 - switch SW2
    MAP_PinTypeGPIO(PIN_15, PIN_MODE_0, false);    // MODE_0 -> GPIO22 (GPIOA2 pin 6)

    // PIN_55 jako USARTA0.TX
    MAP_PinTypeGPIO(PIN_55, PIN_MODE_3, false);    // MODE_3 -> USARTA0.TX

    //// PIN_57 jako USARTA0.RX
    //MAP_PinTypeGPIO(PIN_57, PIN_MODE_3, false);    // MODE_3 -> USARTA0.RX

    // PIN_57 jako GPIO2 - zero crossing detection  (kolizja z USARTA0.RX)
    // UWAGA: �eby sygna� "zero crossing" dociera� do nogi procesora
    // zwora "J6" na launchpad musi byc ustawiona w pozycji "BP"
    MAP_PinTypeGPIO(PIN_57, PIN_MODE_0, false);    // MODE_0 -> GPIO2 (GPIOA0 pin 2)

    // PIN_18 jako GPIO28 - dimmer signal
    MAP_PinTypeGPIO(PIN_18, PIN_MODE_0, false);    // MODE_0 -> GPIO28 (GPIOA3 pin 4)

#endif

#ifdef TARGET_PROS
    // PIN_15 jako GPIO22 - led zielony
    MAP_PinTypeGPIO(PIN_15, PIN_MODE_0, false);    // MODE_0 -> GPIO22 (GPIOA2 pin 6)

    // PIN_62 jako GPIO7 - switch reset
    MAP_PinTypeGPIO(PIN_62, PIN_MODE_0, false);    // MODE_0 -> GPIO7 (GPIOA0 pin 7)

    // PIN_57 jako GPIO2 - zero crossing detection  (kolizja z USARTA0.RX)
    MAP_PinTypeGPIO(PIN_57, PIN_MODE_0, false);    // MODE_0 -> GPIO2 (GPIOA0 pin 2)

    // PIN_18 jako GPIO28 - dimmer signal
    MAP_PinTypeGPIO(PIN_18, PIN_MODE_0, false);    // MODE_0 -> GPIO28 (GPIOA3 pin 4)

    // PIN_55 jako USARTA0.TX
    MAP_PinTypeGPIO(PIN_55, PIN_MODE_3, false);    // MODE_3 -> USARTA0.TX
#endif

}



//*****************************************************************************
// Wysyla na UARTA ci�g znak�w zako�czony 0
void WyslijNapis(const char *str)
{
if(str != NULL)
    {
        while(*str!='\0')
        {
            MAP_UARTCharPut(UARTA0_BASE,*str++);
        }
    }
}



//*****************************************************************************
// Wysyla na UARTA zadan� ilo� bajt�w
void WyslijBajty(const char *str, int ile)
{
while (ile>0)
        {
            MAP_UARTCharPut(UARTA0_BASE,*str++);
            ile--;
        }
}



void CzyBlad(const char *str, int ret)
{
	if (ret >= 0) return;

	char nap[10];

	ZgasWszystkie();
	ZapalZielony();

	sprintf(nap,"(%i)",ret);
	WyslijNapis(nap);
	WyslijNapis(str);

	while (1){}
}




//*****************************************************************************
// Funkcja kt�ra zapala lamp� czyli ustawia wype�nienie na 100%
void ZapalLampe()
{
	g_wypelnienie = 100;
	g_wypelnienie_nastawa = 0;  // jako znacznik �e nie mam zapuszcza� timera w przerwaniu "zero crossing"
#ifdef TARGET_LAUNCHPAD
	ZapalCzerwony();
#endif

#ifdef TARGET_PROS
#endif

	MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0xFF);  // zapalam lampe

}




//*****************************************************************************
// Funkcja kt�ra gasi lamp� czyli ustawia wype�nienie na 0%
void ZgasLampe()
{
	g_wypelnienie = 0;
	g_wypelnienie_nastawa = 0;  // jako znacznik �e nie mam zapuszcza� timera w przerwaniu "zero crossing"

#ifdef TARGET_LAUNCHPAD
	ZgasCzerwony();
#endif

#ifdef TARGET_PROS
#endif

	MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0);  // gasze lampe

}




//*****************************************************************************
// Funkcja kt�ra ustawia wypalnienie na warto�� "wype�nienie"
void UstawLampe(char wypelnienie)
{
	int nastawa;

	g_wypelnienie = wypelnienie;

	// abym nie musia� tego wykonywac co 10ms w przerwaniu licz� to raz tutaj

	// je�li wypelenienie jest r�wne 100% lub 0% to ustawiam wartosc nastawy na zero
	// jako flage �e nie mam wog�le zapuszczac timera w przerwaniu "zero crossing"
	if      (wypelnienie>99) ZapalLampe();
	else if (wypelnienie == 0) ZgasLampe();
	else {
		// po pierwsz� robi� zale�no�� nieliniow� aby bardziej by�o widac zmiany nat�enia (przeskalowane na 0-10000)
		//nastawa = wypelnienie * wypelnienie;
		nastawa = wypelnienie * 100;

		// zalezno�� jest odwtna: czym wi�ksze wype�nienie tym kt�rtszy czas kt�ry ma odliczy� timer
		// wartosc wyrazona w us (jeden okres to 10000us)
		nastawa = 10000 - nastawa;

		// teoretycznie maks nastawa to powinno by� 10 ms ale po tym jak licz� zawsze od zbocza opadaj�cego kt�re mo�e by� oddalone nawet o
		// 0,4ms od rzeczywistego czasu przej�cia przez zero i przy za�o�eniu �e zapalam lini� "dimmer" na 0,1ms to maks robi mi si� 0 0,5ms mniejszy
		if (nastawa > 9500) nastawa = 9500;

		// przechodz� z us na to co trzeba wpisa� w rejestrze timera (1ms = 80000, 1us=80)
		nastawa = nastawa * 80;

		g_wypelnienie_nastawa = nastawa;
	}

}





//***********************************************************************************************************************
// Definicje funkcji - 4 callbacki dla biblioteki simplelink
//***********************************************************************************************************************

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)    // sl_WlanEvtHdlr (user.h) // _SlDrvHandleWlanEvents (simplelink.h)
{
	 if(!pWlanEvent) return;

	 char nap[50];

	 switch(pWlanEvent->Event)
	    {
	        case SL_WLAN_CONNECT_EVENT:
	        {
	            //SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);

	            //
	            // Information about the connected AP (like name, MAC etc) will be
	            // available in 'slWlanConnectAsyncResponse_t'
	            // Applications can use it if required
	            //
	            //  slWlanConnectAsyncResponse_t *pEventData = NULL;
	            // pEventData = &pWlanEvent->EventData.STAandP2PModeWlanConnected;
	            //

	            // Copy new connection SSID and BSSID to global parameters
	            memcpy(g_ucConnectionSSID,
	                   pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_name,
	                   pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_len);
	            memcpy(g_ucConnectionBSSID,
	                   pWlanEvent->EventData.STAandP2PModeWlanConnected.bssid,
	                   SL_BSSID_LENGTH);

	            //ZapalPomaranczowy();   // KOSA - zapalam pomara�czowy led jako znak �e polaczyl sie do sieci
	            g_polaczenie = POLACZENIE_CZEKA_NA_IP;

	            WyslijNapis("[WLAN EVENT] Connected. \n\r");
	            sprintf(nap,"  AP=%s , BSSID= %x:%x:%x:%x:%x:%x\n\r",
	            		 g_ucConnectionSSID,
						 g_ucConnectionBSSID[0], g_ucConnectionBSSID[1], g_ucConnectionBSSID[2],
	            	     g_ucConnectionBSSID[3], g_ucConnectionBSSID[4], g_ucConnectionBSSID[5]);
	            WyslijNapis(nap);

	        }
	        break;

	        case SL_WLAN_DISCONNECT_EVENT:
	        {
	            slWlanConnectAsyncResponse_t*  pEventData = NULL;

	            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
	            //CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

	            pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

	            // If the user has initiated 'Disconnect' request,
	            //'reason_code' is SL_USER_INITIATED_DISCONNECTION
	            if(SL_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
	            {
	            	WyslijNapis("[WLAN EVENT] Disconnected on request. \n\r");
	            	sprintf(nap,"  AP=%s , BSSID= %x:%x:%x:%x:%x:%x\n\r",
	            		    g_ucConnectionSSID,
							g_ucConnectionBSSID[0], g_ucConnectionBSSID[1], g_ucConnectionBSSID[2],
	            		    g_ucConnectionBSSID[3], g_ucConnectionBSSID[4], g_ucConnectionBSSID[5]);
	            	WyslijNapis(nap);
	            }
	            else
	            {
	            	WyslijNapis("[WLAN EVENT] Disconnected on ERROR. \n\r");
	              	sprintf(nap,"  AP=%s , BSSID= %x:%x:%x:%x:%x:%x\n\r",
	            		    g_ucConnectionSSID,
							g_ucConnectionBSSID[0], g_ucConnectionBSSID[1], g_ucConnectionBSSID[2],
	            		    g_ucConnectionBSSID[3], g_ucConnectionBSSID[4], g_ucConnectionBSSID[5]);
	               	WyslijNapis(nap);
	            }

	            memset(g_ucConnectionSSID,0,sizeof(g_ucConnectionSSID));
	            memset(g_ucConnectionBSSID,0,sizeof(g_ucConnectionBSSID));
	        }
	        break;

	        // KOSA - event �e dosta� konfiguracj�
	        case SL_WLAN_SMART_CONFIG_COMPLETE_EVENT:
	                {
	                	//ZapalCzerwony();   // KOSA - zapalam czerwony ze odebral smartconfiga
	                    g_polaczenie = POLACZENIE_LACZY;


	                	WyslijNapis("[WLAN_EVENT] Smartconfig complete!");
	                    sprintf(nap,"  Status=%x ",pWlanEvent->EventData.smartConfigStartResponse.status);
	                    WyslijNapis(nap);
	                    sprintf(nap,"  SSID=%s ",pWlanEvent->EventData.smartConfigStartResponse.ssid);
	                    WyslijNapis(nap);
	                    sprintf(nap,"  PrivateToken=%s\n\r",pWlanEvent->EventData.smartConfigStartResponse.private_token);
	                    WyslijNapis(nap);
	                }
	                break;

	        // KOSA - event �e co� nie tak ze smartconfigiem?
	        case SL_WLAN_SMART_CONFIG_STOP_EVENT:
	                {
	                	WyslijNapis("[WLAN_EVENT] Smartconfig STOP!");
	                    sprintf(nap,"  Status=%x \n\r",pWlanEvent->EventData.smartConfigStopResponse.status);
	                    WyslijNapis(nap);
	                }
	                break;

	        default:
	        {
	            sprintf(nap,"[WLAN EVENT] Unexpected event [0x%x]\n\r",pWlanEvent->Event);
	            WyslijNapis(nap);
	        }
	        break;
	    }
}




//*****************************************************************************
// Zdarzenia zwiazane z sieci� ale nie zwi�zane z wifi
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
	 if(!pNetAppEvent) return;

	 char nap[50];

	 switch(pNetAppEvent->Event)
	    {
	        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
	        {
	            SlIpV4AcquiredAsync_t *pEventData = NULL;

	            //SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

	            //Ip Acquired Event Data
	            pEventData = &pNetAppEvent->EventData.ipAcquiredV4;

	            //Gateway IP address
	            //g_ulGatewayIP = pEventData->gateway;

	            // KOSA - zapalam zielony ze dostal IP od DHCP
	            //ZapalZielony();
	           	g_polaczenie = POLACZENIE_NAWIAZANE;


	            // wy�uskuj� adres IP i go zapami�tuj� - wykorzystyuj� p�niej jako jeden z parametr� ststusu urz�dzenia
	            sprintf(g_AdresIP,"%d.%d.%d.%d",
	            	    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,3),
	            		SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,2),
	            		SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,1),
	            		SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,0));


	            WyslijNapis("[NETAPP EVENT] IP Acquired.\r\n");

	            sprintf(nap,"  IP=%d.%d.%d.%d , GATE=%d.%d.%d.%d\n\r",
	          	            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,3),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,2),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,1),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,0),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,3),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,2),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,1),
							SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,0));

	            WyslijNapis(nap);
	        }
	        break;

	        default:
	        {
	            sprintf(nap,"[NETAPP EVENT] Unexpected event [0x%x] \n\r",pNetAppEvent->Event);
	            WyslijNapis(nap);
	        }
	        break;
	    }
}



unsigned char PROS_token[] = "__SL_P_ROS";


void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pSlHttpServerEvent,
                                  SlHttpServerResponse_t *pSlHttpServerResponse)
{

	   unsigned char strLenVal = 0;

	   WyslijNapis("Odpalil HttpServerCallback.\n\r");

	   if(!pSlHttpServerEvent || !pSlHttpServerResponse)
	    {
	        return;
	    }

	    switch (pSlHttpServerEvent->Event)
	    {
	        case SL_NETAPP_HTTPGETTOKENVALUE_EVENT:
	        {

	        	WyslijNapis("Odebralem HTTP GET.\n\r");

	        	// w to miejsce musz� wkopiowa� dane
	        	char* dst = pSlHttpServerResponse->ResponseData.token_value.data;
	        	// sk�d wzi�� dane
	        	char* src;
                char str_wypelnienie[10];
	        	char len;  // w pSlHttpServerResponse->ResponseData.token_value.data jest przygotowane maksymalnie 64 bajty miejsca!

	            // "type": "bulb"
	            if(memcmp(pSlHttpServerEvent->EventData.httpTokenName.data, "__SL_G_TYP",10) == 0) src = g_TypUrzadzenia;
	            // "id": "0001"
	            if(memcmp(pSlHttpServerEvent->EventData.httpTokenName.data, "__SL_G_IDD",10) == 0) src = g_IDUrzadzenia;
	            // "ip": "192.168.1.1"
	            if(memcmp(pSlHttpServerEvent->EventData.httpTokenName.data, "__SL_G_IPP",10) == 0) src = g_AdresIP;
	            // "dim": 58
	            if(memcmp(pSlHttpServerEvent->EventData.httpTokenName.data, "__SL_G_DIM",10) == 0) {
	            	sprintf(str_wypelnienie,"%i",(int)g_wypelnienie);
	            	src = str_wypelnienie;
	            }

	            len = strlen(src);
	            memcpy(dst,src,len);
	            pSlHttpServerResponse->ResponseData.token_value.len = len;


	          /*
	          unsigned char status, *ptr;

	          ptr = pSlHttpServerResponse->ResponseData.token_value.data;
	          pSlHttpServerResponse->ResponseData.token_value.len = 0;
	          if(memcmp(pSlHttpServerEvent->EventData.httpTokenName.data, GET_token,
	                    strlen((const char *)GET_token)) == 0)
	          {

	        	status = GPIO_IF_LedStatus(MCU_RED_LED_GPIO);
	            strLenVal = strlen(LED1_STRING);
	            memcpy(ptr, LED1_STRING, strLenVal);
	            ptr += strLenVal;
	            pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            if(status & 0x01)
	            {
	              strLenVal = strlen(LED_ON_STRING);
	              memcpy(ptr, LED_ON_STRING, strLenVal);
	              ptr += strLenVal;
	              pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            }
	            else
	            {
	              strLenVal = strlen(LED_OFF_STRING);
	              memcpy(ptr, LED_OFF_STRING, strLenVal);
	              ptr += strLenVal;
	              pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            }
	            status = GPIO_IF_LedStatus(MCU_GREEN_LED_GPIO);
	            strLenVal = strlen(LED2_STRING);
	            memcpy(ptr, LED2_STRING, strLenVal);
	            ptr += strLenVal;
	            pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            if(status & 0x01)
	            {
	              strLenVal = strlen(LED_ON_STRING);
	              memcpy(ptr, LED_ON_STRING, strLenVal);
	              ptr += strLenVal;
	              pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            }
	            else
	            {
	              strLenVal = strlen(LED_OFF_STRING);
	              memcpy(ptr, LED_OFF_STRING, strLenVal);
	              ptr += strLenVal;
	              pSlHttpServerResponse->ResponseData.token_value.len += strLenVal;
	            }
	            *ptr = '\0';
	          }
              */
	        }
	        break;

	        case SL_NETAPP_HTTPPOSTTOKENVALUE_EVENT:
	        {
	           // URL
	           //WyslijBajty(pSlHttpServerEvent->EventData.httpTokenName.data,pSlHttpServerEvent->EventData.httpTokenName.len);
	           //WyslijNapis("\n\r");

	           // URL np. "/bulb/api/v1/on"
	           //WyslijBajty(pSlHttpServerEvent->EventData.httpPostData.action.data,pSlHttpServerEvent->EventData.httpPostData.action.len);
	           //WyslijNapis("\n\r");

	           // nazwa tokena np. "__SL_P_ROS"
	           //WyslijBajty(pSlHttpServerEvent->EventData.httpPostData.token_name.data,pSlHttpServerEvent->EventData.httpPostData.token_name.len);
	           //WyslijNapis("\n\r");

	           // warto�� tokena np. "ok"
	           //WyslijBajty(pSlHttpServerEvent->EventData.httpPostData.token_value.data,pSlHttpServerEvent->EventData.httpPostData.token_value.len);
	           //WyslijNapis("\n\r");

	          WyslijNapis("Odebralem HTTP POST.\n\r");

	          //unsigned char led;
	          unsigned char *ptr = pSlHttpServerEvent->EventData.httpPostData.token_name.data;

	          // sprawdzam czy to jest nasz token
	          if(memcmp(ptr, PROS_token, strlen((const char *)PROS_token)) == 0)
	          {
	            ptr = pSlHttpServerEvent->EventData.httpPostData.token_value.data;

	            // sprawdzam czy wartos� tokena jest taka jak powinna
	            if(memcmp(ptr, "ok", 2) == 0)
	            {
	            	ptr = pSlHttpServerEvent->EventData.httpPostData.action.data;

	            	// sprawdzam czy URL jest taki jaki powinie by�
	            	if(memcmp(ptr, "/bulb/api/v1", 12) == 0)
	            	{

	            		// teraz musz� odnale�c co jest za ostatnim znakiem '/'
	            		// je�li "on" to w��czam lamp�
	            		// je�li "off" to wy��czam lamp�
	            		// nie mog� skorzysta� z funkcji ze standardowej biblioteki bo stringi zwracane przez api nie s� null terminated
	            		unsigned char* p = ptr + pSlHttpServerEvent->EventData.httpPostData.action.len;
	            		while (*p != '/') p--;
	            		p++;
	            		if(memcmp(p, "on", 2) == 0)
	            		{
	            			ZapalLampe();
	            			WyslijNapis("Wlaczam lampe.\r\n");
	            		}
	            		if(memcmp(p, "off", 3) == 0)
	            	    {
		            		ZgasLampe();
	            			WyslijNapis("Wylaczam lampe.\r\n");
	            	    }
	            		if(memcmp(p, "dim=", 4) == 0)
	            		{

	            			p+=4;  // ustawiam si� na pierwsz� cyfr�
	            			char procent = atoi(p);
	               			WyslijNapis("Sciemniam lampe. P=");
	               			char str[10];
	               			sprintf(str,"%i\r\n",procent);
	               			WyslijNapis(str);
	               			UstawLampe(procent);
	            		}

	            	}   // koniec if(memcmp(ptr, "/bulb/api/v1", 12) == 0)

	            }   // koniec if(memcmp(ptr, "ok", 2) == 0)

	          }   // koniec if(memcmp(ptr, PROS_token, strlen((const char *)PROS_token)) == 0)

	        }  // koniec case SL_NETAPP_HTTPPOSTTOKENVALUE_EVENT
	          break;
	        default:
	          break;
	    }



}

void SimpleLinkSockEventHandler(SlSockEvent_t *pSock) {}


//void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent) {}








//***********************************************************************************************************************
// PRZERWANIA
//***********************************************************************************************************************

#ifdef DEBUG_PRZERWANIA
char debug_indeks = 1;
int debug_timerA0[256] = {0};
int debug_timerA1[256] = {0};
char debug_gdzie[256] = {0};
//char debug_ris[256] = {0};
//char debug_mis[256] = {0};
#endif

int licznik_przerwan = 0;



//*****************************************************************************
// Przerwanie od Timera A0
// Ten timer wykorzystywany jest do odmierzania czasu pomi�dzy wykryciem "zero crossing"
// a momentem kiedy nale�y ustawi� wysoko lini� steruj�c� triakiem
// Czas ten b�dzie tym d�u�szy im mniejsze jest wype�nienie
/*
static void TimerA0IntHandler(void)
{
    unsigned long ulStatus;

    if (g_wypelnienie_nastawa != 0) {
    	// wysterowuj� lini� za��czaj�ca triaka
    	MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0xFF);  // zapalam lampe

    	// ... i dodatkowo dla launchpad zapalam czerwon� diod�
#ifdef TARGET_LAUNCHPAD
    	ZapalCzerwony();
#endif
    }


    // clear flagi przerwania
    ulStatus = MAP_TimerIntStatus(TIMERA0_BASE, true);
    MAP_TimerIntClear(TIMERA0_BASE, ulStatus);

    if (debug_indeks < 250) {
    //    	    	debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
    //    	    	debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
    //    	    	debug_gdzie[debug_indeks] = 4;
    //    			debug_indeks++;
        	}

    // nie weim czy to potrzebne - pisali w technical reference manual �e zdj�cie flagi przerwania mo�e chwil� potrwa�
    //MAP_UtilsDelay(16000 * 0.01);   // 0.01 ms
}
*/





//*****************************************************************************
// Przerwanie od Timera A0
// Ten timer wykorzystywany jest do odmierzania czasu pomi�dzy wykryciem "zero crossing"
// a momentem kiedy nale�y ustawi� wysoko lini� steruj�c� triakiem
// (czas ten b�dzie tym d�u�szy im mniejsze jest wype�nienie)
// Dodatkowo od wersji 0.1.4 ten licznik jest te� wykorzystywane do gaszenia linii steruj�cej triakiem.
// (wcze�niej gaszenie by�o dopiero przerwaniu "zero crossing" ale jak pzrerwanie
// zero crossing dzia�a na zbocze opadaj�ce to to by�o za p�no aby gasi� lini� steruj�c� triakiem)
// Ustawiam czas wysterowania linii steruj�cej triakiem na 0,1ms.
static void TimerA0IntHandler(void)
{
    unsigned long ulStatus;


    // clear flagi przerwania
   	ulStatus = MAP_TimerIntStatus(TIMERA0_BASE, true);
   	MAP_TimerIntClear(TIMERA0_BASE, ulStatus);


    if (g_stan_triaka == 0) {
    	if (g_wypelnienie_nastawa != 0) {
    		// wysterowuj� lini� za��czaj�ca triaka
    		MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0xFF);  // zapalam lampe
    		g_stan_triaka = 1;

    		// ustawiam za jaki czas linia sterownia triaka ma p�j�� w d�
    		MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, 0.1 * 80000);    // 2ms
    		MAP_TimerEnable(TIMERA0_BASE, TIMER_A);

    		// ... i dodatkowo dla launchpad zapalam czerwon� diod�
			#ifdef TARGET_LAUNCHPAD
    			ZapalCzerwony();
			#endif
    	}

		#ifdef DEBUG_PRZERWANIA
    	if (debug_indeks < 250) {
    		debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
    		debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
        	debug_gdzie[debug_indeks] = 4;
    		debug_indeks++;
        }
		#endif

    	// nie weim czy to potrzebne - pisali w technical reference manual �e zdj�cie flagi przerwania mo�e chwil� potrwa�
    	//MAP_UtilsDelay(16000 * 0.01);   // 0.01 ms
    }
    // je�li g_stan_triaka == 1 tzn �e teraz mam go ustawi� na zero
    else {
    	MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0);  // gasze sterownie lampy
    	g_stan_triaka = 0;

		#ifdef DEBUG_PRZERWANIA
   	  	// Zapuszczam tutaj timer A0 tylko do cel�w debugowych
    	// abym wiedzai� ile czasu up�ywa pomi�dzy kolejnymi przerwaniami od lini "zero crossing"
    	// Tu ustawiam 9.9ms , razem z 0.1ms ustawionymi wczesniej to warto�� odczytana
    	// w przerwaniu od zero crossing powina by�: odczyt = nastawa + 0.1 + 9.9 - 10 = nastawa
    	MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, 9.9 * 80000);    // 8ms
    	MAP_TimerEnable(TIMERA0_BASE, TIMER_A);

    	if (debug_indeks < 250) {
    		debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
    		debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
    	  	debug_gdzie[debug_indeks] = 5;
    	  	debug_indeks++;
        }
		#endif

    }

}




//*****************************************************************************
// Przerwanie od Timera A1
// Ten timer wykorzystywane jest do odmierzania czasu po kt�rym nale�y odblokowa�
// przerwania od GPIOA3.4 )P18

static void TimerA1IntHandler(void)
{
    unsigned long ulStatus;

    // wlaczam przerwanie od linii detekcji zera
    //MAP_GPIOIntClear  (GPIOA3_BASE, GPIO_INT_PIN_4);   // GPIOA3.4
    //MAP_GPIOIntEnable (GPIOA3_BASE, GPIO_INT_PIN_4);
    MAP_GPIOIntClear  (GPIOA0_BASE, GPIO_INT_PIN_2);   // GPIOA0.2
    MAP_GPIOIntEnable (GPIOA0_BASE, GPIO_INT_PIN_2);

    // clear flagi przerwania
    ulStatus = MAP_TimerIntStatus(TIMERA1_BASE, true);
    MAP_TimerIntClear(TIMERA1_BASE, ulStatus);

	#ifdef DEBUG_PRZERWANIA
    if (debug_indeks < 250) {
    	debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
    	debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
       	debug_gdzie[debug_indeks] = 3;
    	debug_indeks++;
    }
	#endif


    // nie weim czy to potrzebne - pisali w technical reference manual �e zdj�cie flagi przerwania mo�e chwil� potrwa�
    //MAP_UtilsDelay(16000 * 0.01);   // 0.01 ms

}



static void GPIOA0IntHandler(void)
{
	// jakbym chcia� od razu odblokowac przerwania
	//MAP_GPIOIntClear  (GPIOA3_BASE, GPIO_INT_PIN_4);

	//MAP_UtilsDelay(16000 * 2);   // 2 ms

	if (g_wypelnienie_nastawa != 0) {
		// gasze sterownie lampy
		// m�g�bym to robi� wcze�niej (bo to za�aczenia triaka potrzebuj� tylko impuls) ale czy jest sens zapuszcza� specjalnie jeszcze jaki� timer?
		MAP_GPIOPinWrite(GPIOA3_BASE, GPIO_PIN_4, 0);  // gasze sterownie lempy
		g_stan_triaka = 0;

		// dla launchpad musze wy��czy� diod� symuluj�c� lamp� bo ona si� sama nie wy��czy tak jak triak
#ifdef TARGET_LAUNCHPAD
		ZgasCzerwony();
#endif
	}

	// wylaczam przerwanie od linii detekcji zera - zostanie ono w��czone dopiero po pewnym czasie
	//MAP_GPIOIntDisable  (GPIOA3_BASE,  GPIO_INT_PIN_4);  // GPIOA3.4
	//MAP_GPIOIntClear  (GPIOA3_BASE, GPIO_INT_PIN_4);
	MAP_GPIOIntDisable  (GPIOA0_BASE,  GPIO_INT_PIN_2);   // GPIOA0.2
	MAP_GPIOIntClear  (GPIOA0_BASE, GPIO_INT_PIN_2);


	// startuj� licznik kt�ry za 1,5ms wlaczy przerwanie od liniii detekcji zera
	MAP_TimerEnable(TIMERA1_BASE, TIMER_A);

	// inkrementuj� licznik przerwa�
	licznik_przerwan++;

	#ifdef DEBUG_PRZERWANIA
	if (debug_indeks < 250) {
		debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
		debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
	   	debug_gdzie[debug_indeks] = 1;
		debug_indeks++;
	}
	#endif

	// startuj� licznik kt�ry za jaki� czas za��czy triaka (wartos� nastawy wyliczona w UstawLampe())
	if (g_wypelnienie_nastawa != 0) {
		MAP_TimerDisable(TIMERA0_BASE, TIMER_A);
		MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, g_wypelnienie_nastawa);
		MAP_TimerEnable(TIMERA0_BASE, TIMER_A);
	}

    // migam diod� z czetsotliowscia 1 sek
    if ((licznik_przerwan/50) & 0x01) ZapalPomaranczowy();
    else ZgasPomaranczowy();

	#ifdef DEBUG_PRZERWANIA
    if (debug_indeks < 250) {
    	debug_timerA0[debug_indeks] = MAP_TimerValueGet (TIMERA0_BASE, TIMER_A ) / 80;    // przechodz� na us
    	debug_timerA1[debug_indeks] = MAP_TimerValueGet (TIMERA1_BASE, TIMER_A ) / 80;    // przechodz� na us
       	debug_gdzie[debug_indeks] = 2;
    	debug_indeks++;
    }
	#endif

    // nie weim czy to potrzebne - pisali w technical reference manual �e zdj�cie flagi przerwania mo�e chwil� potrwa�
    //MAP_UtilsDelay(16000 * 0.01);   // 0.01 ms

}








//***********************************************************************************************************************
// MAIN
//***********************************************************************************************************************

SlSecParams_t DEBUG_SecParams;
SlGetSecParamsExt_t DEBUG_SecParamsExt;
_u8 DEBUG_MacAddress[6];

int main(void) {
	char napis[50];
	long RetVal;        // jak jest ujemna warto�� to error
    char wystartuj_smartconfig = 1;  // je�li nie b�dzie zapami�tanych �adnych profili to automatycznei startuje smartconfig

	// Podajemy kontrolerowi przerwa� (NVIC) gdzie jest tablica wektorow przerwan
	MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);

    // Odblokowujemy przerwania
	MAP_IntMasterEnable();
	// W�aczamy przerwanie od
    MAP_IntEnable(FAULT_SYSTICK);

    // Jakas podstawowa konfiguracja mikrokontrolera ktora musi byc przeprowadzona na poczatku
    PRCMCC3200MCUInit();

    // doprowadzenie zagara do uzywanych peryferiow
    ClockConfig();

    // uart do wypluwania informacji na terminal
    UARTConfig();

    // gpio - ledy
    GPIOConfig();

    // konfiguracja pinow tak aby odpowiednie peryferia mialy wyjscie na swiat
    PinMuxConfig();

    ZapalWszystkie();
    UtilsDelay(8000000);
    ZgasWszystkie();
    UtilsDelay(1000000);

    WyslijNapis(" ***** Aplikacja ProsBulb ( ");
    WyslijNapis(WERSJA_FIRMWARE);
#ifdef TARGET_PROS
    WyslijNapis(" PROS ) ***** \n\r");
#endif
#ifdef TARGET_LAUNCHPAD
    WyslijNapis(" LAUNCHPAD ) ***** \n\r");
#endif


    // konfiguracja i odplenie timera kt�ry s�y�y do debuga
    //  MAP_TimerConfigure(TIMERA0_BASE, TIMER_CFG_PERIODIC_UP);
    //  MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, 0xFFFFFFFF);  // wartos przy ktorej przeladowuje licznik
    //  MAP_TimerEnable(TIMERA0_BASE, TIMER_A);

    // Konfiguracja timeraA0 kt�ry s�y�y do odmierzania czasu pomi�dzy "zero crossing" a odpaleniem triaka
    MAP_TimerConfigure(TIMERA0_BASE, TIMER_CFG_ONE_SHOT);
    MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, (0 * 80000)  );   //0 ms
    MAP_TimerIntRegister(TIMERA0_BASE, TIMER_A, TimerA0IntHandler);
    MAP_TimerIntClear(TIMERA0_BASE, 0xFF);
    MAP_TimerIntEnable(TIMERA0_BASE, TIMER_TIMA_TIMEOUT);
    // w��czam pzrerwania od TIMERA0 w NVIC
    MAP_IntEnable(INT_TIMERA0A);


    // konfiguracja timera kt�ry b�dzie wykorzystywany do odbierzania czasu mi�dzy przerwaniem od
    // deteksji zera a ponownym za��czeniem tego przerwania
    // Chocia� teoretycznie powinienem dostawa� przerwania tylko od rising edge w rzeczywisto�ci wygl�da, �e
    // dostaj� te� przerwania od falling edge, dlatego tutaj zabezpieczam si� przed tym aby obs�ugiwac tylko
    // r�wno oddalone przerwania (to jest zabepieczenei aby dzia�a� tylk an jedno zbocze)
    // a naszej p�ycie g��wnej by�o tak �e ma�em przerwania co 9.2ms lub 10ms lub 10.8ms co oznacza �e
    // "szpilka" od detekcji zera trwa�a 0.8ms i raz czasy by�y odmierzane od r�nych moment�w co by�o wida� na �wiecenie �ar�wki
    // Jak si� da takie zabezmieczenie to wymuszam to �e b�d� reagowa� na to drugie zbocze co powoduje �e wy��czanie
    // starowania triaka b�dzie robione za p�no dlatego rozi�zane jest to tak �e w momenie za��czenia sterowania
    // triaka zapuszczam timer i po 0.1ms zaeruj� sterowanie triaka (nie tak jak by�o wcze�niej �e zerowanie traiaka by�o w przerwaniu "zero crossing"
    MAP_TimerConfigure(TIMERA1_BASE, TIMER_CFG_ONE_SHOT);
    MAP_TimerLoadSet(TIMERA1_BASE, TIMER_A, (9.8 * 80000)  );   //9.8 ms
    MAP_TimerIntRegister(TIMERA1_BASE, TIMER_A, TimerA1IntHandler);
    MAP_TimerIntClear(TIMERA1_BASE, 0xFF);
    MAP_TimerIntEnable(TIMERA1_BASE, TIMER_TIMA_TIMEOUT);
    // w��czam pzrerwania od TIMERA1 w NVIC
    MAP_IntEnable(INT_TIMERA1A);


    // konfiguruj� przerwanie na opadaj�ce zbocze od linii detekcji zera (P57 GPIOA0.2)
    MAP_GPIOIntRegister(GPIOA0_BASE, GPIOA0IntHandler);
    MAP_GPIOIntTypeSet  (GPIOA0_BASE, 0x02, GPIO_RISING_EDGE);
    //MAP_GPIOIntTypeSet  (GPIOA0_BASE, 0x02, GPIO_FALLING_EDGE);
    //MAP_GPIOIntTypeSet  (GPIOA0_BASE, 0x02, GPIO_BOTH_EDGES);
    MAP_GPIOIntClear  ( GPIOA0_BASE, GPIO_INT_PIN_2);
    MAP_GPIOIntEnable  (GPIOA0_BASE,  GPIO_INT_PIN_2);
    MAP_IntEnable(INT_GPIOA0);



    // Ustawiam zmienna globaln� �e jestem w stanie laczenia z AP
    // bo jak tylko wystartuje SimpleLinka to on bedzie laczyl bo jest w stanie AutoConnect
    g_polaczenie = POLACZENIE_LACZY;

    // To musi by pierwsza funkcja wywo�ana dla simplelinka.
    RetVal = sl_Start(NULL, NULL, NULL);
    CzyBlad("sl_Start failed!\n\r",RetVal);


    WyslijNapis("sl_Start ok.\n\r");



    // Connection profiles
    // Moze byc ich siedem, kazdy profil okresla jeden AP (ssid,password,rodzaj szyfrowania,priorytet)
    // To nie jest konieczne ale wtedy wiemy �e nie ma �adnych zapami�tanych AP i je�li si� po��czymy do jakiego� AP
    // to na 100% b�dzie to ten AP kt�ry dosatli�my Smartconfigiem
    //RetVal = sl_WlanProfileDel(0xFF);  // 0xFF - kasuj wszystkie profile
    //CzyBlad("sl_WlanProfileDel failed!\n\r",RetVal);


    // ***** DEBUG *****
    // Gdy chc� wymusi� po��czenie do danego AP i znam SSID oraz key
    //RetVal = sl_WlanProfileDel(0xFF);  // 0xFF - kasuj wszystkie profile
    //CzyBlad("sl_WlanProfileDel failed!\n\r",RetVal);

    //SlSecParams_t secParams = {0};
    //secParams.Key = (signed char*)("kosa1234");
    //secParams.KeyLen = strlen("kosa1234");
    //secParams.Type = SL_SEC_TYPE_WPA;

    //RetVal = sl_WlanProfileAdd ("axis_konstr",strlen("axis_konstr"),0,&secParams,0,0,0);  // priorrtet = 0
    //CzyBlad("sl_WlanProfileAdd failed!\n\r",RetVal);
    // ***** DEBUG *****



    // Connection Policy
    // Mo�e by� ustawiona na:
    // AUTO - wtedy wykorzystywane s� "Connection Profiles", smartlink ��czy sie z AP kt�ry ma najwy�szy priorytet
    // FAST - ��czy do ostatnio po��czonego
    // AUTOSMARTCONFIG - automatycznie ��czy do tego co dosta� ze smartconfiga???
    int i;
    WyslijNapis("Zapamietane profile:\n\r");
    for (i=0;i<7;i++) {
    	_i16 dl;
    	_u32 priority;

    	sprintf(napis,"%i) ",i);
    	WyslijNapis(napis);

    	RetVal = sl_WlanProfileGet(i,(_i8*)napis,&dl,DEBUG_MacAddress,&DEBUG_SecParams,&DEBUG_SecParamsExt,&priority);
    	if (RetVal < 0) {
    		WyslijNapis("-1\n\r");
    		continue;
    	}

    	// jest przynajmniej jeden zapami�tany profil wi�c nie startuj� smartconfiga z automatu
    	wystartuj_smartconfig = 0;

    	WyslijBajty(napis,dl);
    	sprintf(napis," (%i) \n\r",priority);
        WyslijNapis(napis);
    }



    // Polityka ��czania z AP - ustawiona an AUTO
    //[in]	Type	   Type of policy to be modified. The Options are SL_POLICY_CONNECTION, SL_POLICY_SCAN, SL_POLICY_PM, SL_POLICY_P2P
    //[in]	Policy	   The option value which depends on action type
    //[in]	pVal	   An optional value pointer
    //[in]	ValLen	   An optional value length, in bytes
    //RetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(1,0,0,0,1), NULL, 0 );
    RetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(1,0,0,0,0), NULL, 0 );
    CzyBlad("sl_WlanPolicySet failed!\n\r",RetVal);





    // KOSA
    // Aby nada� nazw� naszemu urz�dzeniu
    //sl_NetAppSet (SL_NET_APP_DEVICE_CONFIG_ID, NETAPP_SET_GET_DEV_CONF_OPT_DOMAIN_NAME,
    //               strlen((const char *)"KosaYes"), (_u8 *)"KosaYes");


    // Za chwile bede rejestrowal usluge mDNS, wiec musz wiedziec jak� nazw� w sieci b�dzie si� przedstawi�o nasze urz�dzenie
    // Nazwa w sieci to: <typ>_<id> np. bulb_1234 lub dim_0012

    // Otwieram plik gdzie spodziewam si� jednej lini postaci: <typ>_<id>
    long DeviceFileHandle;
    RetVal = sl_FsOpen((unsigned char *)"/pros/id.txt",FS_MODE_OPEN_READ,NULL, &DeviceFileHandle);

    if (RetVal >= 0) {
    	RetVal = sl_FsRead( DeviceFileHandle, 0, (unsigned char *)g_cServiceName, SERVICE_NAME_LENGTH);
    	g_cServiceName[RetVal] = 0;   // null terminator
    }
    else {
    	strcpy(g_cServiceName,"bulb_0000");
    }

    // Wy�uskuj� z pliku /pros/id.txt typ urz�dzenia i id urz�dzenia
    char* ptr = strchr(g_cServiceName,'_');
    // je�li nie znalaz��m znaku '_' to zak�adam defaultowo �e jest to bulb
    if (ptr == NULL) {
    	strcpy(g_TypUrzadzenia,"bulb");
    	strncpy(g_IDUrzadzenia,g_cServiceName,ID_URZADZENIA_LEN_MAX);
    }
    // je�li znalaz�em '_' to dziel� na typ i id
    else {
    	int n = ptr-g_cServiceName;
    	if (n>TYP_URZADZENIA_LEN_MAX) n = TYP_URZADZENIA_LEN_MAX;
    	strncpy(g_TypUrzadzenia,g_cServiceName,n);
    	strncpy(g_IDUrzadzenia,ptr+1,ID_URZADZENIA_LEN_MAX);
    }

    // doklejam cze�� sta��
    strcat(g_cServiceName, g_ccServiceName);


    // Rejestracj us�ugi mDNS

    WyslijNapis("Rejestruje usluge mDNS: ");
    WyslijNapis(g_cServiceName);
    WyslijNapis("\n\r");


    RetVal = sl_NetAppStop(SL_NET_APP_MDNS_ID);
    // Jak pr�buj� stopowa� a nie jest wystartowany to dostaj� RetVal=-6
    //CzyBlad("sl_NetAppStop(SL_NET_APP_MDNS_ID) failed!\n\r",RetVal);

    // Jak ustawi� czasy dla atvertise jest w: d:\pros\CC3200\TICC31000...
    // UWAGA: Nawet jak wyrejestruj� wszystkie wcze�niej zarejestrowane serwisy to jeden serwis zawsze chodzi: F4B85EC8F77D@mysimplelink
    RetVal = sl_NetAppMDNSUnRegisterService(0, 0); // to powinno usun�� wszystkie serwisy
  	//RetVal = sl_NetAppMDNSUnRegisterService(SERVICE_NAME,(unsigned char)strlen(SERVICE_NAME));
    CzyBlad("sl_NetAppMDNSUnRegisterService failed!\n\r",RetVal);

    RetVal = sl_NetAppMDNSRegisterService(g_cServiceName,(unsigned char)strlen(g_cServiceName),"pros_id",(unsigned char)strlen("pros_id"),200,2000,1);
    CzyBlad("sl_NetAppMDNSRegisterService failed!\n\r",RetVal);

    RetVal = sl_NetAppStart(SL_NET_APP_MDNS_ID);
    CzyBlad("sl_NetAppStart(SL_NET_APP_MDNS_ID) failed!\n\r",RetVal);

    WyslijNapis("MDNS Registration successful\n\r");


    // Start SmartConfig
    // This example uses the unsecured SmartConfig method
//    RetVal = sl_WlanSmartConfigStart(0,                     /*groupIdBitmask*/
//                               SMART_CONFIG_CIPHER_NONE,    /*cipher*/
//                               0,                           /*publicKeyLen*/
//                               0,                           /*group1KeyLen*/
//                               0,                           /*group2KeyLen */
//                               NULL,                        /*publicKey */
//                               NULL,                        /*group1Key */
//                               NULL);                       /*group2Key*/
//    CzyBlad("sl_WlanSmartConfigStart failed!\n\r",RetVal);
//
//    WyslijNapis("Smartconfig wystartowal.\n\r");


//        // Wait for WLAN Event
//        while((!IS_CONNECTED(g_ulStatus)) || (!IS_IP_ACQUIRED(g_ulStatus)))
//        {
//            _SlNonOsMainLoopTask();
//        }
//         //
//         // Turn ON the RED LED to indicate connection success
//         //
//         GPIO_IF_LedOn(MCU_RED_LED_GPIO);
//         //wait for few moments
//         MAP_UtilsDelay(80000000);
//         //reset to default AUTO policy
//         lRetVal = sl_WlanPolicySet(  SL_POLICY_CONNECTION,
//                               SL_CONNECTION_POLICY(1,0,0,0,0),
//                               &policyVal,
//                               1 /*PolicyValLen*/);
//         ASSERT_ON_ERROR(lRetVal);


    //Stop Internal HTTP Server
    RetVal = sl_NetAppStop(SL_NET_APP_HTTP_SERVER_ID);
    //CzyBlad("sl_NetAppStop(HTTP_SERVER) failed!\n\r",RetVal);

    //Start Internal HTTP Server
    RetVal = sl_NetAppStart(SL_NET_APP_HTTP_SERVER_ID);
    CzyBlad("sl_NetAppStart(HTTP_SERVER) failed!\n\r",RetVal);

    WyslijNapis("HTTP Server started\n\r");





    int licznik_dioda = 0;

    while (1) {
    	_SlNonOsMainLoopTask();
    	licznik_dioda++;

    	if (licznik_dioda == 1) ZapalDiodeStatus();

    	//if (CzytajPrzycisk()) ZapalDiodeStatus();
    	//else ZgasDiodeStatus();

    	if ((wystartuj_smartconfig) || CzytajPrzycisk()) {

    		// debounce
    		if (!wystartuj_smartconfig) {
    			int i;
    			wystartuj_smartconfig = 1;
    			for (i=0;i<100;i++) {
    				if (!CzytajPrzycisk()) wystartuj_smartconfig = 0;
    				UtilsDelay(10000);
    			}
    		}

    		if (wystartuj_smartconfig) {
    			wystartuj_smartconfig = 0;
    			// Nacisniecie przycisku powoduje:
    			// - roz�aczenie z sieci� (je�li jestem po��czony)
    			// - wykasowanie zapami�tanych profili (czy to potrzebne???)
    			// - ustawianie polityki na autoconect
    			// - wystartowanie smartconfiga
    			ZgasDiodeStatus();
    			g_polaczenie = POLACZENIE_NASLUCHUJE;

    			WyslijNapis("Nacisnieto przycisk!\n\r");

    			// Jak jestem polaczony to zwraca zero a jak nie to inna wartosc
    			RetVal = sl_WlanDisconnect();
    			sprintf(napis,"WlanDisconnect = %i",RetVal);

    			RetVal = sl_WlanProfileDel(0xFF);  // usuwam wszystkie zapamietane profile
    			CzyBlad("sl_WlanProfileDel failed!\n\r",RetVal);

    			//set AUTO policy
    			RetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
    		                              SL_CONNECTION_POLICY(1,0,0,0,0),
    		                              NULL,  //&policyVal,
    		                              0   /*1*/ /*PolicyValLen*/);
    			CzyBlad("sl_WlanPolicySet failed!\n\r",RetVal);

    			RetVal = sl_WlanSmartConfigStart(0,                /*groupIdBitmask*/
    		                           SMART_CONFIG_CIPHER_NONE,    /*cipher*/
    		                           0,                           /*publicKeyLen*/
    		                           0,                           /*group1KeyLen*/
    		                           0,                           /*group2KeyLen */
    		                           NULL,                        /*publicKey */
    		                           NULL,                        /*group1Key */
    		                           NULL);                       /*group2Key*/
    			CzyBlad("sl_WlanSmartConfigStart failed!\n\r",RetVal);
    			WyslijNapis("Smartconfig wystartowal.\n\r");

    			// Aby nie by�o tak �e przycisk naci�nie si� dwa razy z rz�du
    			UtilsDelay(10000000);
    		}
    	}

       	if (licznik_dioda == 20000) {
    		if ((g_polaczenie == POLACZENIE_NASLUCHUJE) || (g_polaczenie == POLACZENIE_LACZY) || (g_polaczenie == POLACZENIE_CZEKA_NA_IP)) ZgasDiodeStatus();
    	}

    	if (licznik_dioda == 40000) {
    		if ((g_polaczenie == POLACZENIE_LACZY) || (g_polaczenie == POLACZENIE_CZEKA_NA_IP)) ZapalDiodeStatus();
    	}
    	if (licznik_dioda == 60000) {
    		if ((g_polaczenie == POLACZENIE_LACZY) || (g_polaczenie == POLACZENIE_CZEKA_NA_IP)) ZgasDiodeStatus();
    	}
    	if (licznik_dioda == 80000) {
        	if ((g_polaczenie == POLACZENIE_CZEKA_NA_IP)) ZapalDiodeStatus();
        }
    	if (licznik_dioda == 10000) {
            if ((g_polaczenie == POLACZENIE_CZEKA_NA_IP)) ZgasDiodeStatus();
        }

    	if (licznik_dioda >= 1000000) {
			#ifdef DEBUG_PRZERWANIA
    			if (debug_indeks == 250) {
    				char nap[30];

    				for (i=0;i<debug_indeks;i++) {
    					sprintf(nap,"%i %5i %5i \r\n",debug_gdzie[i],debug_timerA0[i],debug_timerA1[i]);   // gdzie,timer od triaka,timer od odblokowania przerwan
    					WyslijNapis(nap);
    				}
    				sprintf(nap,"ILOSC PRZERWAN = %i \r\n",licznik_przerwan);
    				WyslijNapis(nap);

    				debug_indeks = 1;  // jako znacznik ze znow mozna zapamietywac przerwania

    			}
			#endif

    		licznik_dioda = 0;
    	}
    }

}







//   Testy z zapisywanie i odczytywaniem pliku
//    char*           DeviceFileName = "/kosa/test.txt"; //"MyFile.txt";
//    unsigned long   MaxSize = 63 * 1024; //62.5K is max file size
//    long            DeviceFileHandle = -1;
//    unsigned long   Offset = 0;
//    unsigned char   InputBuffer[100];
//
//
//    // Create a file and write data. The file in this example is secured, without signature and with a fail safe commit
//   // RetVal = sl_FsOpen((unsigned char *)DeviceFileName,
//   //                                  FS_MODE_OPEN_CREATE(MaxSize , _FS_FILE_OPEN_FLAG_NO_SIGNATURE_TEST | _FS_FILE_OPEN_FLAG_COMMIT ),
//   //                                  NULL, &DeviceFileHandle);
//   // RetVal = sl_FsOpen((unsigned char *)DeviceFileName,FS_MODE_OPEN_CREATE(MaxSize , 0 ),NULL, &DeviceFileHandle);
//
//   // Offset = 0;
//    //Preferred in secure file that the Offset and the length will be aligned to 16 bytes.
//   // RetVal = sl_FsWrite(DeviceFileHandle, Offset, (unsigned char *)"HelloWorld", strlen("HelloWorld"));
//
//   // RetVal = sl_FsClose(DeviceFileHandle, NULL, NULL , 0);
//
//    // open the same file for read, using the Token we got from the creation procedure above
//    //RetVal = sl_FsOpen((unsigned char *)DeviceFileName,FS_MODE_OPEN_READ,NULL, &DeviceFileHandle);
//    //RetVal = sl_FsOpen((unsigned char *)"/sys/stacfg.ini",FS_MODE_OPEN_READ,NULL, &DeviceFileHandle);
//    RetVal = sl_FsOpen((unsigned char *)"/sys/devname.cfg",FS_MODE_OPEN_READ,NULL, &DeviceFileHandle);
//    if(RetVal < 0)
//        {
//       	char nap[30];
//    	ZapalCzerwony();
//       	sprintf(nap,"Open error: %i\n\r",RetVal);
//       	WyslijNapis(nap);
//       	while(1){}
//        }
//
//   	WyslijNapis("Plik otwarty \n\r");
//
//
//    Offset = 0;
//
//    // przesy�am zawartosc pliku na uarta
//    do {
//    	RetVal = sl_FsRead( DeviceFileHandle, Offset, (unsigned char *)InputBuffer, 20);
//    	WyslijBajty(InputBuffer,RetVal);
//    }
//    while (RetVal == 20);
//
//    WyslijNapis("\n\r-------\n\r");
//
//    RetVal = sl_FsClose(DeviceFileHandle, NULL, NULL , 0);
//    if(RetVal < 0)
//           {
//           char nap[30];
//       	   ZapalCzerwony();
//           sprintf(nap,"Close error: %i\n\r",RetVal);
//           WyslijNapis(nap);
//           while(1){}
//           }
//
//    WyslijNapis("Plik zamkniety \n\r");

